﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileSearch
{
    public partial class Form1 : Form
    {
        private int _files_cnt;

        private IEnumerator<string> _files_enum;

        private readonly Stopwatch _sw = new Stopwatch();

        private CancellationTokenSource _cts = new CancellationTokenSource();

        private readonly string _file_path;

        public CancellationToken CancellationToken
        {
            get
            {
                _cts.Token.Register(() => _cts = new CancellationTokenSource());
                return _cts.Token;
            }
        }

        public Form1()
        {
            InitializeComponent();

            var dir = Environment.CurrentDirectory;
            var file = Path.Combine(dir, "settings.xml");
            _file_path = file;
        }

        private void StopEventRegistration()
        {
            btn_Stop.Click += (s, ea) =>
            {
                _cts.Cancel();
                _sw.Stop();
                lbl_secs.Text = _sw.Elapsed.TotalSeconds.ToString("#");
                btn_cont.Enabled = true;
            };
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StopEventRegistration();
            var set = GetSettings();
            SetSettings(set);
        }

        private void SetSettings(Settings settings)
        {
            tb_InitDir.Text = settings.StartDir;
            tb_TmplFileName.Text = settings.FilePattern;
            tb_FileText.Text = settings.InnerText;
        }

        private Settings GetSettings()
        {
            Settings settings = null;
            try
            {
                settings = Settings.LoadFromFile(_file_path);
            }
            catch(Exception ex)
            {
                settings = new Settings()
                {
                    FilePattern = "*.*"
                };
            }
            return settings;
        }

        private void Search(string pattern, CancellationToken cancel)
        {
            if (_files_enum == null) return;

            string file_txt = string.Empty;

            while (_files_enum.MoveNext())
            {
                if (cancel.IsCancellationRequested)
                {
                    return;
                }

                string fname = _files_enum.Current;

                lbl_CurFile.Invoke(new Action(() =>
                {
                    lbl_CurFile.Text = string.Format("({0}) {1}", ++_files_cnt, fname);
                }
                ));

                file_txt = File.ReadAllText(fname, System.Text.Encoding.UTF8);

                if (file_txt.Contains(pattern))
                {
                    treeView1.Invoke(new Action(() =>
                    {
                        var keys = fname.Split('\\');

                        var nodes = treeView1.Nodes;

                        foreach (var key in keys)
                        {
                            var node = nodes.Find(key, false);
                            if (!node.Any())
                            {
                                var newNode = new TreeNode { Name = key, Text = key };
                                nodes.Add(newNode);
                            }
                            nodes = nodes.Find(key, false)[0].Nodes;
                        }

                        treeView1.ExpandAll();
                    }
                    ));
                }
            }
        }

        private async void btn_NewSearch_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tb_InitDir.Text) || string.IsNullOrEmpty(tb_FileText.Text) || string.IsNullOrEmpty(tb_TmplFileName.Text))
            {
                MessageBox.Show("Укажите все критерии поиска!");
                return;
            }

            btn_NewSearch.Enabled = false;
            btn_cont.Enabled = false;
            treeView1.Nodes.Clear();

            _files_cnt = 0;
            lbl_secs.Text = "";

            try
            {
                _files_enum = Directory.EnumerateFiles(tb_InitDir.Text, tb_TmplFileName.Text, SearchOption.AllDirectories).GetEnumerator();
                _sw.Restart();
                await SearchAsync(tb_FileText.Text);
            }
            finally
            {
                _sw.Stop();
                lbl_secs.Text = _sw.Elapsed.TotalSeconds.ToString("#");
                btn_NewSearch.Enabled = true;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tb_InitDir.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private async Task SearchAsync(string pattern)
        {
            await Task.Run(() => Search(pattern, CancellationToken));
        }

        private async void btn_cont_Click(object sender, EventArgs e)
        {
            btn_NewSearch.Enabled = false;
            btn_cont.Enabled = false;
            try
            {
                _sw.Start();
                await SearchAsync(tb_FileText.Text);
            }
            finally
            {
                _sw.Stop();
                lbl_secs.Text = _sw.Elapsed.TotalSeconds.ToString("#");
                btn_NewSearch.Enabled = true;
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                new Settings()
                {
                    StartDir = tb_InitDir.Text,
                    FilePattern = tb_TmplFileName.Text,
                    InnerText = tb_FileText.Text
                }.SaveToFile(_file_path);
            }
            catch { }
        }
    }
}
