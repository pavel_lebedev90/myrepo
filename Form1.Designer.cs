﻿namespace FileSearch
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_cont = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.btn_NewSearch = new System.Windows.Forms.Button();
            this.tb_FileText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_TmplFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_InitDir = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lbl_CurFile = new System.Windows.Forms.Label();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_secs = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_cont);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.btn_Stop);
            this.groupBox1.Controls.Add(this.btn_NewSearch);
            this.groupBox1.Controls.Add(this.tb_FileText);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_TmplFileName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tb_InitDir);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(629, 163);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Критерии поиска";
            // 
            // btn_cont
            // 
            this.btn_cont.Enabled = false;
            this.btn_cont.Location = new System.Drawing.Point(248, 134);
            this.btn_cont.Name = "btn_cont";
            this.btn_cont.Size = new System.Drawing.Size(113, 23);
            this.btn_cont.TabIndex = 9;
            this.btn_cont.Text = "Продолжить";
            this.btn_cont.UseVisualStyleBackColor = true;
            this.btn_cont.Click += new System.EventHandler(this.btn_cont_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 18);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(130, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Стартовая директория";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(129, 134);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(113, 23);
            this.btn_Stop.TabIndex = 7;
            this.btn_Stop.Text = "Остановить";
            this.btn_Stop.UseVisualStyleBackColor = true;
            // 
            // btn_NewSearch
            // 
            this.btn_NewSearch.Location = new System.Drawing.Point(10, 134);
            this.btn_NewSearch.Name = "btn_NewSearch";
            this.btn_NewSearch.Size = new System.Drawing.Size(113, 23);
            this.btn_NewSearch.TabIndex = 6;
            this.btn_NewSearch.Text = "Новый поиск";
            this.btn_NewSearch.UseVisualStyleBackColor = true;
            this.btn_NewSearch.Click += new System.EventHandler(this.btn_NewSearch_Click);
            // 
            // tb_FileText
            // 
            this.tb_FileText.Location = new System.Drawing.Point(177, 82);
            this.tb_FileText.Name = "tb_FileText";
            this.tb_FileText.Size = new System.Drawing.Size(446, 20);
            this.tb_FileText.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Текст, содержащийся в файле";
            // 
            // tb_TmplFileName
            // 
            this.tb_TmplFileName.Location = new System.Drawing.Point(135, 50);
            this.tb_TmplFileName.Name = "tb_TmplFileName";
            this.tb_TmplFileName.Size = new System.Drawing.Size(488, 20);
            this.tb_TmplFileName.TabIndex = 3;
            this.tb_TmplFileName.Text = "*.*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Шаблон имени файла";
            // 
            // tb_InitDir
            // 
            this.tb_InitDir.Enabled = false;
            this.tb_InitDir.Location = new System.Drawing.Point(146, 21);
            this.tb_InitDir.Name = "tb_InitDir";
            this.tb_InitDir.Size = new System.Drawing.Size(477, 20);
            this.tb_InitDir.TabIndex = 1;
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(6, 232);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(629, 291);
            this.treeView1.TabIndex = 2;
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.ShowNewFolderButton = false;
            // 
            // lbl_CurFile
            // 
            this.lbl_CurFile.AutoSize = true;
            this.lbl_CurFile.Location = new System.Drawing.Point(12, 190);
            this.lbl_CurFile.Name = "lbl_CurFile";
            this.lbl_CurFile.Size = new System.Drawing.Size(0, 13);
            this.lbl_CurFile.TabIndex = 3;
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Location = new System.Drawing.Point(9, 216);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(97, 13);
            this.lbl_time.TabIndex = 4;
            this.lbl_time.Text = "Время поиска (с):";
            // 
            // lbl_secs
            // 
            this.lbl_secs.AutoSize = true;
            this.lbl_secs.Location = new System.Drawing.Point(112, 216);
            this.lbl_secs.Name = "lbl_secs";
            this.lbl_secs.Size = new System.Drawing.Size(0, 13);
            this.lbl_secs.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 535);
            this.Controls.Add(this.lbl_secs);
            this.Controls.Add(this.lbl_time);
            this.Controls.Add(this.lbl_CurFile);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.Button btn_NewSearch;
        private System.Windows.Forms.TextBox tb_FileText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_TmplFileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_InitDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbl_CurFile;
        private System.Windows.Forms.Button btn_cont;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_secs;
    }
}

