﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace FileSearch
{
    [Serializable]
    public class Settings
    {
        public string StartDir { get; set; }
        public string FilePattern { get; set; }
        public string InnerText { get; set; }

        public static Settings LoadFromFile(string file)
        {
            using (var stream = new StreamReader(file))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Settings));
                return (Settings)ser.Deserialize(stream);
            }
        }

        public void SaveToFile(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
            using (var stream = new StreamWriter(file))
            {
                XmlSerializer ser = new XmlSerializer(typeof(Settings));
                ser.Serialize(stream, this);
            }
        }
    }
}
